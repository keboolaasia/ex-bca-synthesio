VERSION=`git describe --tags`
IMAGE=pocin/kbc-ex-synthesio
TESTCOMMAND="docker run --rm -it --entrypoint '/bin/bash' -v `pwd`:/src/ -e KBC_DATADIR='/src/tests/data/' ${IMAGE}:latest /src/run_tests.sh"
test:
	eval $(TESTCOMMAND)

run:
	docker run --rm -v `pwd`:/src/ -e KBC_DATADIR=/src/tests/data/ ${IMAGE}:latest

sh:
	docker run --rm -it --entrypoint "/bin/bash" -v `pwd`:/src/ -e KBC_DATADIR=/src/tests/data/ ${IMAGE}:latest

prod:
	docker run --rm -it --entrypoint "/bin/bash" -e KBC_DATADIR=/src/data/ ${IMAGE}:latest

build:
	echo "Building ${IMAGE}:${VERSION}"
	docker build -t ${IMAGE}:${VERSION} -t ${IMAGE}:latest .

deploy:
	echo "Pusing to dockerhub"
	docker push ${IMAGE}:${VERSION}
	docker push ${IMAGE}:latest
