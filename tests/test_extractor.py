import pytest
from exsynthesio.extractor import (parse_begin_end, serialize_stream,
                                   OutputFormat, replace_keyword_in_config,
                                   serialize_topics)
from exsynthesio.client import flatten_record
import datetime

def test_parsing_timestamps_utcnow_kw():
    start = 'utcnow'
    hours_delta = '-2'
    begin, end = parse_begin_end(start, hours_delta)
    assert isinstance(begin, str)
    assert isinstance(end, str)

def test_serializing_topics():
    expected = [
        {"tag": "has", "name": "Hashtags", "parent": "NDP"},
        {"tag": "gen", "name": "General Keywords", "parent": "NDP"},
        {"tag": None, "name": "NDP", "parent": None},
        {"tag": "ndpc", "name": "NDPeeps", "parent": "NDPeeps"},
        {"tag": None, "name": "NDPeeps", "parent": None},
        {"tag": "min", "name": "MINDEF", "parent": "SG Gov"},
        {"tag": "rsa", "name": "RSAF", "parent": "SG Gov"},
        {"tag": None, "name": "SG Gov", "parent": None},
        {"tag": "ndr", "name": "NDR 2018", "parent": "National Day Rally"},
        {"tag": None, "name": "National Day Rally", "parent": None}
    ]
    raw = [
        {
            "children": [
                {"children": [], "tag": "has", "name": "Hashtags"},
                {"children": [], "tag": "gen", "name": "General Keywords"}
            ],
            "name": "NDP", "tag": None},
        {
            "children": [
                {"children": [], "tag": "ndpc", "name": "NDPeeps"}
            ],
            "name": "NDPeeps", "tag": None},
        {
            "children": [
                {"children": [], "tag": "min", "name": "MINDEF"},
                {"children": [], "tag": "rsa", "name": "RSAF"}
            ],
            "name": "SG Gov", "tag": None},
        {
            "children": [
                {"children": [], "tag": "ndr", "name": "NDR 2018"}
            ], "name": "National Day Rally", "tag": None}
    ]

    def tupleize(dic):
        return set(map(lambda x: tuple(x.items()), dic))
    assert tupleize(expected) == tupleize(serialize_topics(raw))


def test_parsing_timestamp_iso8601():
    start = '2018-01-03T14:02:12Z'
    hours_delta = -2
    begin, end = parse_begin_end(start, hours_delta)
    assert isinstance(begin, str)
    assert isinstance(end, str)

def test_dumping_json_as_csv(tmpdir):
    outpath = tmpdir.join("output.csv")

    def stream():
        yield {"foo": "bar"}
        yield {"foo": "bar1"}
        yield {"foo": "bar2"}

    serialize_stream(stream(), outpath.strpath, OutputFormat.RAW_JSON_IN_CSV)

    contents = outpath.read()
    expected = """data
"{""foo"": ""bar""}"
"{""foo"": ""bar1""}"
"{""foo"": ""bar2""}"
"""
    assert contents == expected

@pytest.mark.parametrize(
    "cfg,expected",
    [
        (
            {"replace_me": "curse!", "keep": "this"},
            {"replace_me": "new_value", "keep": "this"}
        ),
        (
            {"nested": {"replace_me": "curse!", "keep": "this"}},
            {"nested": {"replace_me": "new_value", "keep": "this"}}
        ),
        (
            {"nested": [{"replace_me": "curse!", "keep": "this"}]},
            {"nested": [{"replace_me": "new_value", "keep": "this"}]}
        )
    ])
def test_replacing_keyword_in_config(cfg, expected):
    replaced = replace_keyword_in_config(cfg, "replace_me", "new_value")
    assert replaced == expected

def test_replacing_keyword_in_config_with_callable():
    cfg = {"foo": 21, "arr": [1,2]}
    def replace_f(x):
        return 2*x

    replaced = replace_keyword_in_config(cfg, "foo", replace_f)
    expected = {"foo": 42, "arr": [1,2]}

    assert replaced == expected


def test_flattening_tree_like_response():
    data = {
        "2018-08-11T02:00:00+0800": {
            "value": 11,
            "data": {"neutral": {"value": 10, "data": {"13": {"value": 6, "data": {"has": {"value": 6}}}, "11": {"value": 3, "data": {"sin": {"value": 2}, "min": {"value": 1}}}, "17": {"value": 1, "data": {"ndpc": {"value": 1}}}}}, "positive": {"value": 1, "data": {"13": {"value": 1, "data": {"has": {"value": 1}}}}
            }
            }
        }
    }
    levels = ['date', 'sentiment', 'site_type', 'tag']
    expected = [
        {'date': '2018-08-11T02:00:00+0800', 'sentiment': 'neutral',
         'site_type': '13', 'tag': 'has', 'value': 6},
        {'date': '2018-08-11T02:00:00+0800', 'sentiment': 'neutral',
         'site_type': '11', 'tag': 'sin', 'value': 2},
        {'date': '2018-08-11T02:00:00+0800', 'sentiment': 'neutral',
         'site_type': '11', 'tag': 'min', 'value': 1},
        {'date': '2018-08-11T02:00:00+0800', 'sentiment': 'neutral',
         'site_type': '17', 'tag': 'ndpc', 'value': 1},
        {'date': '2018-08-11T02:00:00+0800', 'sentiment': 'positive',
         'site_type': '13', 'tag': 'has', 'value': 1}
    ]

    assert expected == list(flatten_record(data, levels))

def test_flattening_empty_response():
    data = {}
    out = list(flatten_record(data, levels=['date', 'sentiment', 'site_type']))
    expected = []
    assert out == expected

