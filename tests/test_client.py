import pytest
import os
from exsynthesio.client import Client

def test_creating_client():
    client = Client('foo', 'bar', 'baz', password='qux')
    assert client._password == 'qux'

def test_getting_access_token():
    auth = {
        'client_id': os.getenv('EX_CLIENT_ID'),
        'client_secret': os.getenv("EX_CLIENT_SECRET"),
        'username': os.getenv("EX_USERNAME"),
        'password': os.getenv("EX_PASSWORD"),
        'security_api_version': 'v2'
    }
    client = Client(**auth)
    first_token = client.access_token
    assert isinstance(first_token, str)
    second_token = client.access_token
    assert first_token == second_token
