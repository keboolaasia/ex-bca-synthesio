#!/bin/env python3
from exsynthesio.extractor import main
import sys
import os
import traceback
from requests.exceptions import ConnectionError, HTTPError
import logging

from json import JSONDecodeError

if __name__ == "__main__":
    try:

        logging.basicConfig(level=logging.INFO, stream=sys.stdout)
        main(datadir=os.getenv("KBC_DATADIR", '/data/'))
    except (ValueError, KeyError, ConnectionError, HTTPError, JSONDecodeError) as err:
        logging.exception("Something went wrong:")
        sys.exit(1)
    except Exception as err:
        logging.exception("Something went wrong, internally")
        traceback.print_exc(file=sys.stderr)
        sys.exit(2)
