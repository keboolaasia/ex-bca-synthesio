"""

A class to comunicate with Synthesio REST API https://rest.synthesio.com/doc/

Handles authentication + specific requests

"""
import logging
from json import JSONDecodeError
import requests
import urllib
import pandas as pd
import csv
from copy import copy, deepcopy

from typing import Dict, Any

class Client:
    """
    Authenticate to the Synthesio REST API
    Provide helper methods to download specific reports

    """

    def __init__(self, client_secret, client_id, username, password, security_api_version='v2'):
        self._client_secret = client_secret
        self._client_id = client_id
        self._username = username
        self._password = password
        self.base_url = 'https://rest.synthesio.com'
        self._access_token = None
        self.security_api_version = security_api_version

    def _get_access_token(self):
        data = {
            "grant_type": "password",
            "username": self._username,
            "password": self._password,
            "client_id": self._client_id,
            "client_secret": self._client_secret,
            "scope": "read",
        }
        print("Authenticating")
        response = requests.post(
            self.base_url + "/security/{}/oauth/token".format(self.security_api_version),
            data=data)
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print(err.response.content)
            raise
        else:
            print("Authenticated")
            return response.json()['access_token']

    @property
    def access_token(self):
        """
        Get existing or fetch new access_token.

        THIS DOES NOT TAKE CARE OF EXPIRATIONS AS the token lives for 10800 secs
        which should be ok

        """
        if self._access_token is None:
            self._access_token = self._get_access_token()
        return self._access_token

    @access_token.setter
    def access_token(self, value):
        self._access_token = value

    def _post(self, url, params=None, payload=None):
        """
        Make a post request, if access_token expires, retry

        """
        try:
            return self.authorized_request("POST", url, params=params, payload=payload)
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 401:
                self.access_token = None
                print("Access token expired, trying again")
                return self.authorized_request("POST", url, params=params, payload=payload)
            else:
                raise

    def authorized_request(self, method, url, params=None, payload=None):
        headers = {
            'Authorization': "Bearer {token}".format(token=self.access_token)
        }
        try:
            resp = requests.request(
                method,
                url,
                json=payload, headers=headers, params=params)
            try:
                json_resp = resp.json()
            except JSONDecodeError:
                raise ValueError(
                    "Could not parse response from server: {}".format(resp.text))

            _errors = json_resp.get('errors')
            if _errors and _errors[0]['status'] != 200:
                raise requests.exceptions.HTTPError(response=resp)
        except requests.exceptions.HTTPError as err:
            print(err.response.text)
            raise
        else:
            return json_resp

    def _get(self, url, params=None):
        """make authorized GET,
        retry on expired token"""
        try:
            return self.authorized_request("GET", url, params=params)
        except requests.exceptions.HTTPError as err:
            if err.response.status_code == 401:
                self.access_token = None
                print("Access token expired, trying again")
                return self.authorized_request("GET", url, params=params)
            else:
                raise


    def mentions_by_city(self,
                         report_id,
                         countries,
                         begin=None,
                         end=None,
                         shortcut=None,
                         human_review_statuses=["reviewed"]):
        """
        Get all mentions for all cities from given country

        This merely returns the cities and number of mentions
        {'data': {'Jetis': {'value': 14},
        'BARAT': {'value': 1},
        'Jatinom': {'value': 1},
        'Pasar Manggis': {'value': 3},
        'Mannuruki': {'value': 8},
        ...
        }
        we use the cities in subsequent calls to fetch the sentiments and stuff

        Args:
            countries: a list of three letter country codes (["IDN"])
            begin: a timestamp in the ISO8601 format "2017-07-25T16:00:00.000Z"
            end: a timestamp in the ISO8601 format "2017-07-25T16:00:00.000Z"
                either use both begin and end, or the `shortcut`
            shortcut: "last-1m"
            reviewed (bool): human review status?

        """
        payload = {
            "aggs": [{
                "field": "author.location.city",
                "size": 0
            }],
            "filters": {
                "period": {},
                "human_review_statuses": human_review_statuses,
                "countries": None
            }
        }
        if not isinstance(countries, list):
            raise ValueError('countries must be a list of three letter country'
                             'codes (i.e. ["IDN"])')
        else:
            payload['filters']['countries'] = countries
        if begin is None and end is None:
            if shortcut is None:
                raise ValueError(
                    "Either specify begin and end parameters OR shortcut")
            else:
                payload['filters']['period'] = {"shortcut": shortcut}
        else:
            payload['filters']['period'] = {'begin': begin, 'end': end}

        url = self.base_url + "/mention/v2/reports/{report_id}/_analytics".format(
            report_id=report_id)
        return self._post(url, payload=payload)

    def search_mentions_in_city(self,
                                cities,
                                begin,
                                end,
                                report_id,
                                medias=[9938]):
        """
        Given a city name and time period find all mentions+stats of it

        Args:
            city_name ([str]): human friendly city name
            begin (str): a timestamp in the ISO8601 format "2017-07-25T16:00:00.000Z"
            end (str): a timestamp in the ISO8601 format "2017-07-25T16:00:00.000Z"
            medias ([int]): array of platform ids 9938 is twitter

        Returns:
            pass
        """
        payload = {
            "filters": {
                "period": {
                    "begin": begin,
                    "end": end
                },
                "medias": {
                    "sites": None
                },
                "author_cities": None
            }
        }

        if not isinstance(cities, list) and len(cities) != 1:
            raise ValueError("cities must be array of 1 city name, not"
                             " '{}' of type '{}'".format(cities, type(cities)))
        else:
            payload['filters']['author_cities'] = cities

        if not isinstance(medias, list):
            raise ValueError("medias must be array of ints, not"
                             " '{}' of type '{}'".format(cities, type(cities)))
        else:
            payload['filters']['medias']['sites'] = medias

        fetch_next_page = True
        size = 1000
        start_from = 0
        hits = None
        results = []
        while fetch_next_page:
            print("getting next page: start=%s size=%s hits=%s" % (start_from, size, hits))
            url = (self.base_url + "/mention/v2/reports/{id}/_search".format(
                id=report_id))
            params = {'size': size, 'from': start_from}
            resp = self._post(url, params=params, payload=payload)
            for tweet in resp['data']:
                row = (tweet['id'], tweet['infused_at'], tweet['sentiment'],
                       cities[0])
                results.append(row)
            try:
                hits = int(resp['meta']['hits'])
                start_from += size
            except KeyError:
                # no more hits?
                print("No more hits? %s" % resp)
                fetch_next_page = False
            else:
                if start_from >= hits:
                    fetch_next_page = False
        return results


    def _analytics(self,
                   payload: Dict[str, Any],
                   report_id: int,
                   params: Dict[str, Any] = None,
                   flatten_tree: bool=False
    ) -> Dict[str, Any]:
        """Use the _analytics endpoint

        Does not use pagination!!
        https://rest.synthesio.com/doc/#analytics

        Args:

        """
        endpoint = "/mention/v2/reports/{id}/_analytics".format(id=report_id)

        url = urllib.parse.urljoin(self.base_url, endpoint.lstrip('/'))
        big_json = self._post(url=url, payload=payload)

        def convert_resp_to_stream(body):
            "well, pseudo stream since it's already in memory"
            if isinstance(body['data'], dict):
                for key, value in body['data'].items():
                    # {"2016-03": {"value": 24004, "data": ...}}
                    yield {key: value}
            elif isinstance(body['data'], list) and len(body['data']) == 0:
                # the dataset is empty, we still want to yield something, though
                # for the sake of compatibility with the downstream processing (flattening)
                # https://stackoverflow.com/a/36658865/4050925
                # this yields empty generator
                yield from ()
            else:
                raise NotImplementedError("Don't know what to do. Response is a list of values", body)

        raw_stream = convert_resp_to_stream(big_json)

        if flatten_tree:
            level_names = self._infer_anayltics_level_names_from_params(payload)
            for raw_record in raw_stream:
                yield from flatten_record(raw_record, levels=level_names)
        else:
            # raw mentions
            yield from raw_stream

    def _infer_anayltics_level_names_from_params(self, params):
        return [param['field'] for param in params['aggs']]

    def _search(self, payload, report_id):
        """
        use the _search endpoint and handle pagination

        Args:
            payload (dict): see https://rest.synthesio.com/doc/#search
            report_id (str): get from the synthesio ui

        Returns:
            a generator which yields a row from the response['data'] field. Paginating included
        """
        endpoint = "/mention/v2/reports/{id}/_search".format(id=report_id)

        yield from self._paginated_post(endpoint=endpoint, payload=payload)

    def _paginated_post(self, endpoint, payload):
        fetch_next_page = True
        size = 1000
        start_from = 0
        hits = None

        url = urllib.parse.urljoin(self.base_url, endpoint.lstrip('/'))

        while fetch_next_page:
            print("getting next page: start=%s size=%s hits=%s" % (start_from, size, hits))

            params = {'size': size, 'from': start_from}
            resp = self._post(url, params=params, payload=payload)
            if isinstance(resp['data'], list):
                for row in resp['data']:
                    yield row
            elif isinstance(resp['data'], dict):
                # the output can be like
                # resp = {"data":{
                #  "2016-03": {
                #   "value": 24004,
                #   "data": {
                #     "neutral": {
                #       "value": 19557
                #     },
                #     "positive": {
                #       "value": 3094
                #         ...
                # so we turn it into one line per level
                for key, value in resp['data'].items():
                    # {"2016-03": {"value": 24004, "data": ...}}
                    yield {key: value}
            try:
                hits = int(resp['meta']['hits'])
                start_from += size
            except KeyError:
                # no more hits?
                print("No more hits? %s" % resp)
                fetch_next_page = False
            else:
                if start_from >= hits:
                    fetch_next_page = False

    def fetch_social_posts(self, report_id, begin, end, sites):
        """

        Arguments:
            sites (list): one of [1650630, 9938] # instagram, twitter

        """
        raw_posts  = {
            "filters": {
                "period": {
                    "begin": begin,
                    "end": end,
                },
                "countries": ["IDN"],
                "medias": {
                    "sites": sites
                },
            }
        }

        yield from self._search(raw_posts, report_id)

    def _get_raw_hourly_sentiment_by_cities_hours(self, report_id, cities,
                                                  begin, end):
        """

        """
        payload = {
            "aggs": [{
                "field": "date",
                "interval": "hour"
            }, {
                "field": "sentiment",
            }, {
                "field": "author.location.city",
                "size": 0
            }],
            "filters": {
                "sentiments": ["positive", "neutral", "negative"],
                "period": {
                    "begin": begin,  #"2018-01-01T00:00:00Z",
                    "end": end,  #"2018-01-02T23:59:59Z"
                },
                "human_review_statuses": ["reviewed"],
                "author_cities": cities
            }
        }
        params = {'size': 1000, 'from': 0}
        url = (self.base_url + "/mention/v2/reports/{id}/_analytics".format(
            id=report_id))

        return self._post(url, payload=payload, params=params)

    def _serialize_raw_mentions_by_city_hour_sentiment(self, data):
        """Clean up balast from the raw response

        Returns:
            a pd.DataFrame with columns
                [date_hour, city_name, positive, negative, neutral]
                this still needs some postprocessing, because it doesn't contain
                all defined cities for all timestamps (can contain holes)
        """
        output = []
        if isinstance(data, list):
            return pd.DataFrame(columns=['negative', 'positive',
                                         'neutral', 'date_hour',
                                         'city_name'])
        for hour in data.keys():
            try:
                for sentiment in data[hour]['data'].keys():
                    for city_name, count in data[hour]['data'][sentiment][
                            'data'].items():
                        row = {}
                        row['date_hour'] = hour
                        row['city_name'] = city_name
                        row[sentiment] = count['value']
                        output.append(row)
            except AttributeError:
                # empty result set apppears as list
                continue
        sentiments_by_city_hour = (
            pd.DataFrame(output).groupby(['date_hour', 'city_name'])
            .sum(axis=1).fillna(0).astype(int).reset_index())
        sentiments_by_city_hour['date_hour'] = pd.to_datetime(
            sentiments_by_city_hour['date_hour'], utc=True)
        return sentiments_by_city_hour

    @staticmethod
    def _augment_mentions_by_city_hour_sentiment(serialized, begin, end,
                                                 cities):
        """
        Get rid of the sparsity in the raw data + assign city ids + fill missing columns

        Args:
            serialized (pd.DataFrame): result of self._serialize_raw_mentions_by_city_hour_sentiment
            cities (pd.DataFrame): loaded dataframe (csv) provided by sapient
            begin (str): ISO8601 UTC timestamp
            end (str): ISO8601 UTC timestamp
        """

        required_columns = {'positive', 'negative', 'neutral'}
        has_columns = set(serialized.columns.tolist())
        missing_columns = required_columns.difference(has_columns)
        for col in missing_columns:
            print("Column '{}' is missing! Adding as 0".format(col))
            serialized.loc[:, col] = 0

        serialized_all_cities = (
            serialized.merge(
                cities[['name', 'id']],
                left_on='city_name',
                right_on='name',
                how='right').drop(
                    'city_name', axis=1)
            .rename(columns={'id': 'city_id',
                             'name': 'city_name'}))

        def _fill_mising_dates(df, begin, end):
            new_index = pd.date_range(start=begin, end=end, freq='H')
            tmp = (df.set_index('date_hour').reindex(new_index)
                   .drop(['city_name', 'city_id'], 1))
            tmp.fillna(0, inplace=True)  # replace missing tweets with 0
            return tmp

        dense = (serialized_all_cities.groupby(['city_name', 'city_id']).apply(
            _fill_mising_dates, begin=begin, end=end))

        dense.index.names = ['city_name', 'city_id', 'date_hour']

        jakarta_time = (dense.index.get_level_values('date_hour')
                        .tz_convert('Asia/Jakarta')
                        .strftime("%Y-%m-%d %H:%M:%S"))

        dense.index = dense.index.set_levels(jakarta_time, level='date_hour')

        return dense

    def get_hourly_sentiment_by_cities(self, report_id, cities, begin, end):
        """
        Given a list of cities, get sentiments for each city and hour keeping 0s

        """
        raw = self._get_raw_hourly_sentiment_by_cities_hours(
            report_id, cities['name'].tolist(), begin, end)
        serialized = self._serialize_raw_mentions_by_city_hour_sentiment(raw[
            'data'])
        augmented = self._augment_mentions_by_city_hour_sentiment(
            serialized, begin=begin, end=end, cities=cities)
        return augmented




    def _extract_media_urls(self, post):
        urls = {
            'post_image_link': None,
            'post_video_link': None
        }
        try:
            attachments = post['meta']['attachment_list']
        except KeyError:
            # no attachment. Pass is dangerous but should be fine here
            pass
        else:
            for att in attachments:
                if att['type'] == 'video':
                    urls['post_video_link'] = att['url']
                elif att['type'] == 'image':
                    urls['post_image_link'] = att['url']
        return urls


    def _extract_ig_metrics_from_post(self, post):
        metrics = ['likes', 'comments', 'reposts', 'views']
        metrics_data = {
            'post_likes':0,
            'post_comments': 0,
            'post_reposts': 0,
            'post_views': 0
        }
        for metric in post['metric_list']:
            for metric_name in metrics:
                if metric['name'] == metric_name:
                    metrics_data['post_' + metric_name] = metric['value']
        return metrics_data

    def _extract_tw_metrics_from_post(self, post):
        metrics_data = {
            'post_likes':0,
            'post_comments': 0,
            'post_reposts': 0,
            'post_views': 0
        }
        for metric in post['metric_list']:
            if metric['name'] == 'favorites':
                metrics_data['post_likes'] = metric['value']
            elif metric['name'] == 'retweets':
                metrics_data['post_reposts'] = metric['value']
        return metrics_data

    def parse_ig_post(self, post):
        ig_metrics = self._extract_ig_metrics_from_post(post)
        ig_media_urls = self._extract_media_urls(post)

        serialized_post = {
            'post_id': post['id'],
            'post_link': post['url'],
            'post_views': None,
            'post_likes': None,
            'post_comments': None,
            'post_reposts': None,
            'post_sentiment': post['sentiment'],
            'post_image_link': None,
            'post_video_link': None,
            'post_text': post['content'],
            'user_name': None,
            'user_id': post.get('author_id'),
            'user_followers': post['influence'],
            'datetime': post['date'],
            'location_city': post['location'].get('city'),
            'location_lat': post['location'].get('latitude'),
            'location_long': post['location'].get('longitude'),
            'location_country': post['location'].get('country'),
            'synthesio_rank': post['synthesio_rank']
        }

        serialized_post.update(ig_metrics)
        serialized_post.update(ig_media_urls)
        return serialized_post


    def parse_tw_post(self, post):
        metrics = self._extract_tw_metrics_from_post(post)
        media_urls = self._extract_media_urls(post)

        author_id = post.get('author_id')
        post_url = post['url']
        if author_id is None:
            logging.warning('No author_id for post %s', post_url)

        serialized_post = {
            'post_id': post['native_id'],
            'post_link':post_url,
            'post_views': None,
            'post_likes': None,
            'post_comments': None,
            'post_reposts': None,
            'post_sentiment': post['sentiment'],
            'post_image_link': None,
            'post_video_link': None,
            'post_text': post['content'],
            'user_name': None,
            'user_id': author_id,
            'user_followers': post['influence'],
            'datetime': post['infused_at'],
            'location_city': post['location'].get('city') if post['location'] else None,
            'location_country': post['location'].get('country') if post['location'] else None,
            'synthesio_rank': post['synthesio_rank']

        }

        serialized_post.update(metrics)
        serialized_post.update(media_urls)
        return serialized_post


def flatten_record(obj, levels):
    """
    The record is a tree where the parent nodes are not named.
    we traverse the tree to the leaves and record
    i) the leaf value (= the final value)
    ii) the path through the levels (=column names, dimensions in final csv)
    """

    levels = copy(levels)
    curr_level = levels.pop(0)
    for key, value in obj.items():
        # there should be only one date but we loop anyway
        record = {curr_level: key}
        # value['data'] are children of a node
        yield from flatten_level(value['data'], record, levels)

def flatten_level(obj, flattened, levels):
    if isinstance(obj, list):
        if len(obj) > 0:
            raise ValueError(
                    "Something is terribly wrong and the response"
                    "format is not as expected", obj, flattened, levels)
        print(obj, "would contain empty [], imputing None for all dimensions")
        dummy = deepcopy(flattened)
        for col in levels:
            dummy[col] = None
        yield dummy
    else:
        levels = copy(levels)
        curr_level = levels.pop(0)

        for k, v in obj.items():
            this_level_data = flattened.copy()
            this_level_data[curr_level] = k
            if 'data' not in v:
                # this is the last dimension
                this_level_data['value'] = v['value']
                yield this_level_data
            else:
                yield from flatten_level(
                    v['data'],
                    this_level_data,
                    levels)
