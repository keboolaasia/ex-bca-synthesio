import datetime
from pathlib import Path
import os
import json
from keboola import docker
import pandas as pd
import maya
from .client import Client
import csv
import logging
import pytz
import enum
from typing import Union, Iterable, Callable, Any

class OutputFormat(enum.Enum):
    RAW_JSON_IN_CSV = enum.auto()
    NEW_LINE_DELIMITED_JSON = enum.auto()
    DUMB_JSON_TO_CSV = enum.auto()



AUTH_PARAMS = ['#client_secret', '#client_id', 'username', '#password']
PATH_IN_CITIES = 'in/tables/cities.csv'
PATH_OUT_RAW_TW_POSTS = 'out/tables/raw_tw_posts.csv'
PATH_OUT_RAW_IG_POSTS = 'out/tables/raw_ig_posts.csv'
REQUIRED_INPUT_TABLES = [PATH_IN_CITIES]
REQUIRED_PARAMS = AUTH_PARAMS + ['report_id', 'hours_delta', 'begin']

RAW_POSTS_COLUMNS = ['user_id', 'user_followers',
                     'post_sentiment', 'post_reposts',
                     'post_video_link', 'post_comments',
                     'location_city', 'post_image_link',
                     'datetime', 'location_country',
                     'post_text', 'post_id', 'post_views',
                     'synthesio_rank',
                     'post_likes', 'user_name', 'post_link']


def set_up_client(datadir=os.getenv("KBC_DATADIR")):
    print("Parsing config")
    cfg = docker.Config(datadir)
    params = cfg.get_parameters()
    # for p in REQUIRED_PARAMS:
    #     if p not in params.keys():
    #         raise ValueError("Provide '{}' in config".format(p))
    print("Setting up Synthesio client")
    auth = {param.strip("#"): params[param] for param in AUTH_PARAMS}
    sec_api_v = params.get('security_api_version', 'v2')
    print("Using security api", sec_api_v )
    auth['security_api_version'] = sec_api_v
    synthesio = Client(**auth)
    return synthesio, params


def parse_begin_end(timestamp, hours_delta):
    """
    Format a start timestamp and how much we want to look into history,
    for the Synthesio api (ISO8601 format) UTC

    untill the previous hour

    Args:
        timestamp (str): either 'utcnow',
            or a timestsamp in format 'YYYY-MM-DDTHH:MM:ss+XXXX'
        hours_delta (int): how many hours to add to timestamp to produce
            the beginning of the "epoch". Use negative integers

    Returns:
        (begin, end) two strings in UTC
    """
    utcnow = pytz.utc.localize(datetime.datetime.utcnow() + datetime.timedelta(hours=-1)).replace(minute=59, second=59)
    if timestamp == 'utcnow':
        _end = utcnow
    else:
        _end = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%SZ").astimezone(pytz.utc)
    _begin = (_end + datetime.timedelta(hours=int(hours_delta))).replace(
        minute=0, second=0)
    end = _end.strftime("%Y-%m-%dT%H:%M:%SZ")
    begin = _begin.strftime("%Y-%m-%dT%H:%M:%SZ")
    print("Parsing timestamp '%s' and hours_delta '%s' as "
          "begin '%s' and end '%s'" % (timestamp, hours_delta, begin, end))
    return begin, end

def parse_human_datetime(when: str) -> str:
    return maya.when(when).iso8601()




def download_tw_posts(client, report, begin, end, outpath):
    with open(outpath, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=RAW_POSTS_COLUMNS)
        writer.writeheader()
        posts = client.fetch_social_posts(report, begin, end, [9938]) # 9938 for twitter
        for post in posts:
            if post.get('deleted') or post.get('native_id') is None:
                continue
            parsed_post = client.parse_tw_post(post)
            if parsed_post['user_followers'] is None:
                continue
            writer.writerow(parsed_post)
    return outpath

def download_ig_posts(client, report, begin, end, outpath):

    with open(outpath, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=RAW_POSTS_COLUMNS + ['location_lat', 'location_long'])
        writer.writeheader()
        posts = client.fetch_social_posts(report, begin, end, [1650630])
        for post in posts:
            if post.get('deleted'):
                continue
            parsed_post = client.parse_ig_post(post)
            writer.writerow(parsed_post)

    return outpath

def serialize_stream(stream: Iterable,
                     outpath: Union[Path, str],
                     fmt: OutputFormat) -> Path:

    outpath = Path(outpath)
    logging.info("Saving stream to %s as %s", outpath, fmt)
    stream = iter(stream)
    with open(outpath, 'w') as outf:
        if fmt == OutputFormat.RAW_JSON_IN_CSV:
            wr = csv.DictWriter(outf, fieldnames=['data'])
            wr.writeheader()
            for row in stream:
                wr.writerow({"data": json.dumps(row)})
        elif fmt == OutputFormat.NEW_LINE_DELIMITED_JSON:
            outf.writerows(stream)
        elif fmt == OutputFormat.DUMB_JSON_TO_CSV:
            try:
                first_row = next(stream)
            except StopIteration:
                logging.info("Stream is empty, no data to write in %s", outpath)
                # we would end up with an empty file without header - that's not what we want.
                outpath.unlink()
            else:
                wr = csv.DictWriter(outf, fieldnames=first_row.keys())
                wr.writeheader()
                wr.writerow(first_row)
                wr.writerows(stream)
        else:
            raise ValueError("Unkown output format '{}'".format(fmt))

def replace_keyword_in_config(config: dict,
                              replace_key: str,
                              replace_with: Union[Any, Callable[[str], str]]):
    """Replace a value of given key with a new value

    Traverse (potentially) nested dict when a key `replace_key` is found,
    replace it with `replace_with` otherwise copy

    if replace_with is a not a callable, the value is replaced.
    If it's a function, the value at key `replace_key` is used as an argument to the function and it's return value is used in place.
    """
    if isinstance(config, dict):
        return {
            key: (replace_keyword_in_config(value, replace_key, replace_with)
                  if key != replace_key
                  else (replace_with(value) if callable(replace_with) else replace_with))
                for key, value
                in config.items()
        }
    elif isinstance(config, list):
        return [replace_keyword_in_config(obj, replace_key, replace_with) for obj in config]
    else:
        # it's a scalar
        return config

def process_search_queries(search_params: list, client: Client) -> None:
    for search in search_params:
        payload = search['payload']
        print("Doing a search query", search["name"], "for report", search["report_id"])
        print("initial payload is {}".format(payload))
        for keyword in ["begin", "end"]:
            payload = replace_keyword_in_config(payload, keyword, parse_human_datetime)
        print("final payload is {}".format(payload))
        data_stream = client._search(payload, search['report_id'])
        outpath = Path("/data/out/tables/") / "{report_id}_{name}.csv".format(
            report_id=search['report_id'],
            name=Path(search['name']).stem)
        serialize_stream(data_stream, outpath, fmt=OutputFormat.RAW_JSON_IN_CSV)

def process_analytics_queries(analytics_params: list, client: Client) -> None:
    for params in analytics_params:
        payload = params['payload']
        print("Doing a _analytics query", params["name"], "for report", params["report_id"])
        print("initial payload is {}".format(payload))
        for keyword in ["begin", "end"]:
            payload = replace_keyword_in_config(payload, keyword, parse_human_datetime)
        print("final payload is {}".format(payload))
        data_stream = client._analytics(
            payload=payload,
            report_id=params['report_id'],
            flatten_tree=True)
        outpath = Path("/data/out/tables/") / "{report_id}_{name}.csv".format(
            report_id=params['report_id'],
            name=Path(params['name']).stem)
        serialize_stream(data_stream, outpath, fmt=OutputFormat.DUMB_JSON_TO_CSV)

def serialize_topics(raw_topics):
    def walk(children, previous_parent, all_topics):
        # all_topics = {} if all_topics is None else all_topics
        for ch in children:
            all_topics.append({
                'tag': ch['tag'],
                'name': ch['name'],
                'parent': previous_parent
            })
            walk(ch['children'], ch['name'], all_topics)

    all_topics = []
    walk(raw_topics, previous_parent=None, all_topics=all_topics)
    return all_topics

def process_topics(report_id, client: Client):
    print("Downloading topics for report_id", report_id)
    url = "https://rest.synthesio.com/api/v2/report/{}/topics".format(report_id)
    raw_topics = client._get(url)[str(report_id)]
    serialized_topics = serialize_topics(raw_topics)
    outpath = Path("/data/out/tables/") / '{}_topics.csv'.format(report_id)
    serialize_stream(serialized_topics, outpath=outpath, fmt=OutputFormat.DUMB_JSON_TO_CSV)


def process_custom_queries(params: dict, client: Client, datadir: str='/data'):
    # stuff below is custom and will be deprecated, do not use!
    begin, end = parse_begin_end(
        timestamp=params['begin'], hours_delta=params['hours_delta'])

    custom_queries = params.get('what', {})
    if custom_queries.get('sentiment_by_cities'):
        cities = pd.read_csv(os.path.join(datadir, PATH_IN_CITIES))
        twitter_mentions = client.get_hourly_sentiment_by_cities(
            report_id=params['report_id'],
            cities=cities,
            begin=begin,
            end=end)
        outdir = os.path.join(datadir, 'out/tables/', 'tw_cities.csv')
        print("Saving twitter data to %s" % outdir)
        twitter_mentions.to_csv(outdir)

    if custom_queries.get('tw_posts'):
        print("Downloading raw twitter posts")
        raw_tw_posts_out = os.path.join(datadir, PATH_OUT_RAW_TW_POSTS)
        raw_tw_posts = download_tw_posts(client, params['report_id'], begin, end, raw_tw_posts_out)

    if custom_queries.get('ig_posts'):
        print("Downloading raw instagram posts")
        raw_ig_posts_out = os.path.join(datadir, PATH_OUT_RAW_IG_POSTS)
        raw_ig_posts = download_ig_posts(client, params['report_id'], begin, end, raw_ig_posts_out)

def main(datadir):
    synthesio, params = set_up_client(datadir)
    search_params = params.get("search")
    if search_params:
        # do not process the rest if there was at least one search payload
        process_search_queries(search_params, synthesio)

    analytics_params = params.get("analytics")
    if analytics_params:
        process_analytics_queries(analytics_params, synthesio)

    if params.get("what"):
        process_custom_queries(params, synthesio, datadir=datadir)

    cfg_topics = params.get("topics")
    if cfg_topics:
        process_topics(cfg_topics['report_id'], synthesio)
