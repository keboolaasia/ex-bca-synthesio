# Synthesio extractor

## Configuration

```javascript
{
  "parameters": {
    "#client_secret": "",
    "#client_id": "",
    "username": "",
    "#password": "",
    "oauth_security_api_version": "v1",

    "search": [
      {
        "payload": {},
        "report_id": "",
        "name": "resulting_name_of_csv",
      }
    ],

    "analytics": [
      {
        "payload": {},
        "report_id": "",
        "name": "resulting_name_of_csv"
      }
    ]

    "topics": {"report_id": "1234"}
  }
}
```

where
- `payload` contains a json object for [_search](https://rest.synthesio.com/doc/#search) and [_analytics](https://rest.synthesio.com/doc/#analytics) endpoints, respectively
- pagination is handled
- output csv names are a combination of `{report_id}_{name}.csv`
- whenever a `"begin"` or `"end"` key is found in `payload` it's value is parsed into a datetime

- if using `_analytics` the first agg type must be date, i.e (and the output is slightly different than the raw api response (check `Client._paginated_post()` method))
```

  "aggs": [
      { # This must be always the first
          "field": "date",
          "interval": "day",
          "time_zone": "Asia/Singapore"
      },
      {
          "field": "sentiment"
      }
  ]

```

In the end this
```javascript
"payload": {
    "filters": {
      "period": {
          "end": "now utc",
          "begin": "now -7 days utc"
      }
    "aggs": {"whatever": "value"}
  }
}
```
is sent to the api as

```javascript
"payload": {
    "filters": {
      "period": {
          "end": "2018-07-24T17:00:23Z",
          "begin": "2018-07-17T17:00:23Z"
      }
    "aggs": {"whatever": "value"}
  }
}
```
