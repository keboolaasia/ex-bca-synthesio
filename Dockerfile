FROM python:3.6.6-jessie
# RUN apt update && --no-cache --update-cache add \
#     gcc \
#     gfortran \
#     python3-dev \
#     build-base \
#     wget \
#     openblas-dev\
#     ca-certificates \
#     && ln -s /usr/include/locale.h /usr/include/xlocale.h
RUN pip3 install --upgrade pip==18.0 && pip3 install --no-cache-dir \
    pytest \
    requests \
    arrow \
    maya \
    pandas==0.22 \
    https://github.com/keboola/python-docker-application/archive/2.0.0.zip

RUN mkdir -p /data/out/tables /data/in/tables /data/out/files /data/in/files
COPY . /src/

CMD python3 -u /src/main.py
